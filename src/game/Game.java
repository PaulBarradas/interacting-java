/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author Paul Barradas
 */
public class Game
{

    
    public static void main(String[] args) 
    {
        // System objects
        Scanner in = new Scanner(System.in);
        Random rand = new Random();
        
        //game variables
        String[] enemies = { "Hell Hound", "Assassin", "Black Night", "Bright Bomber", "Ghoul Trooper", "Raven" };
        int maxEnemyHealth = 75;
        int enemyAtkDmg = 25;
        
        //player variables
        int health = 200;
        int atkDmg = 50;
        int numbHealthPots = 3;
        int healthPotHealAmmount = 30;
        int healthPotionDropChance = 10;
        int numbCHUGJUG = 1;
        boolean running = true;
        
        GAME:
        while(running)
        {
            System.out.println("---------------------------------------------------------------------------------------");
            System.out.println("Welcome to Hell! you will fight untill you die, but you will try to defeat "
                    + "as many \nof the creatures that you find here. Fight for your brothers, with honor, AND PRIDE!!!!");
            System.out.println("---------------------------------------------------------------------------------------");
            
            int enemyHealth = rand.nextInt(maxEnemyHealth);
            String enemy = enemies[rand.nextInt(enemies.length)];
            System.out.println("\t # " + enemy + " has appeared! #\n");
            

        
            while(enemyHealth > 0)
            {
                System.out.println("\n\n\t###############################");
                System.out.println("\t# Your HP: " + health + " #");
                System.out.println("\t# " + enemy + "'s HP: " + enemyHealth + " #");
                System.out.println("\n\t# What would you like to do? #");
                System.out.println("\t# 1. Attack #");
                System.out.println("\t# 2. Drink health potion #");
                System.out.println("\t# 3. Drink a CHUG JUG! #");
                System.out.println("\t###############################");
                String input = in.nextLine();
                
                if(input.equals("1"))
                {
                    int damageDealt = rand.nextInt(atkDmg);
                    int damageTaken = rand.nextInt(enemyAtkDmg);
                    enemyHealth -= damageDealt;
                    health -= damageTaken;
                    System.out.println("\t> You strike the " + enemy + " for " + damageDealt + " damage.");
                    System.out.println("\t> You you recieve " + damageTaken + " in retaliaton!");
                    
                    if(health < 1)
                    {
                        System.out.println("\t> You have taken too much damage, you are too weak to go on.");
                        break;
                    }
                }
                else if(input.equals("2"))
                {
                     if(numbHealthPots > 0)
                     {
                         health += healthPotHealAmmount;
                         numbHealthPots --;
                         System.out.println("you have drinked a health potion healing yourself for "+ healthPotHealAmmount +"."
                                            + "\n\t you now have" + health + " HP."
                                            + "\n\t you have now " + numbHealthPots + "health potions left. \n");
                     }
                     else
                     {
                         System.out.println("\t You have no Health potions left!");
                     }
                             
                }
                else if(input.equals("3"))
                {
                     if(numbCHUGJUG > 0)
                     {
                         health = 200;
                         numbCHUGJUG --;
                         System.out.println("\tyou have drinked a legendary CHUG JUG!! healing you to full HP!!");
                     }
                     else
                     {
                         System.out.println("\tYou have no more CHUG JUGS left!!");
                     }
                }
                else 
                {
                     System.out.println("\tInvalid command!");
                }
            }
            if(health < 1)
            {
                System.out.println("\t> You have been defeated, try again next time!");
                break;
            }
            System.out.println("-----------------------------------");
            System.out.println("++ " + enemy +  " was defeated! ++");
            System.out.println("++ you have " + health + "HP left. ++");
            if(rand.nextInt(100)< healthPotionDropChance)
            {
                numbHealthPots ++;
                System.out.println(" # The " + enemy + " has dropped a health potion! # ");
                System.out.println(" # you now have " + numbHealthPots + " health potion(s). # ");
            }
            System.out.println("-----------------------------------");
            System.out.println("are you ready for the next enemy?");
            System.out.println("1. Lets fight another one shall we?");
            
            String input = in.nextLine();
            
            while(!input.equals("1"))
            {
                System.out.println("Invalid command");
                input = in.nextLine();
            }
            
            if(input.equals("1"))
            {
                System.out.println("Go get em tiger!!");
            } 
            
        }
        System.out.println("\n\n\t######################");
        System.out.println("\t# THANKS FOR PLAYING #");
        System.out.println("\t######################");
    }
    
}
